window.onload = function() {

    var root = document.location + "/../file/";
    $('#modelType').selectpicker('val', 'Model');
    $('#APIVersion').selectpicker('val', '1');


    $(".lang-btn").on("click", function() {
      $(".lang-btn").removeClass("active");
      $(this).toggleClass("active");
    });


    var className, lang, json, baseClassName, map, noImport, picker;
    var result;

    $("#parse-json").on("click", function() {

      var inputType = $(".tab-links li.active").attr("input-type")

      switch (inputType) {
        case "json":
          inputJson()
          break;
        case "server":
          inputServer()
          break;
        case "store":
          inputStore()
          break;
        case "file":
          inputFile()
          break;
        default:
          setError("Nessun tipo input trovato");
      }

      $(".right-col div").removeClass("hidden");
    });


    // INPUT TYPE FUNCTION

    function inputFile() {

        if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
          alert('The File APIs are not fully supported in this browser.');
          return;
        }   

        input = document.getElementById('myfile');
        if (!input) {
          alert("Um, couldn't find the fileinput element.");
        }
        else if (!input.files) {
          alert("This browser doesn't seem to support the `files` property of file inputs.");
        }
        else if (!input.files[0]) {
          alert("Please select a file before clicking 'Load'");               
        }
        else {
            var text = ''
            var reader = new FileReader();
            reader.onload = function() {
                text = reader.result;
                convertFileText(text)
            };
            reader.readAsText(input.files[0]);
        }
    }
    
     
    function convertFileText(text) {
        className = getClassName()
        lang = $(".lang-btn.active").attr("lang");
        json = $("#json-string").val();
        // Options
        baseClassName = $("#extends").val();
        map = $("#mapping").is(':checked');
        noImport = !$("#import").is(':checked');
        modelType = getModelType();

        var data = {
          "className" : className,
          "lang" : lang,
          "json" : "",
          "fileTxt" : text,
          "baseClassName" : baseClassName,
          "map" : map,
          "noImport" : noImport,
          "modelType" : modelType,
          "type" : "file"
        };

        result = new Parser().parse(data)
        $("#json-string-preview").text(result)
        setSuccess();
    }
    
    
    function inputJson() {
      // Obbligatori
      className = getClassName();
      lang = $(".lang-btn.active").attr("lang");
      json = $("#json-string").val();
      // Options
      baseClassName = $("#extends").val();
      map = $("#mapping").is(':checked');
      noImport = !$("#import").is(':checked');
      modelType = getModelType();
        
      var check = checkParams(className, json, lang);
      if (check["success"] == true) {

        var data = {
          "className" : className,
          "lang" : lang,
          "json" : json,
          "fileTxt" : "",
          "baseClassName" : baseClassName,
          "map" : map,
          "noImport" : noImport,
          "modelType" : modelType,
          "type" : "json"
        };

        result = new Parser().parse(data);
        $("#json-string-preview").text(result)
        setSuccess();
      }
      else {
        setError(check["error"]);
      }

    }




    function inputServer() {
      var url = $('#server').val();
      url = url.camelize();
      url = url.capitalizeFirstLetter();

      if (url.length == 0) {
          setError("Inserire un URL corretto");
      }
      else {

        var params = {
            "smartapp" : $("#smartapp").is(':checked'),
            "APIVersion" : $("#APIVersion").val(),
            "method" : document.querySelector('input[name="method"]:checked').value,
        }

        httpGetAsync(url, params, function(response) {
          console.log(response);
        });
      }
    }

    function inputStore() {
      console.log("input store");
    }



    $("#save-file").on("click", function() {
        var inputType = $(".tab-links li.active").attr("input-type")
        if (inputType == "file") {
            
            var filepath = root + className + "." + lang
            var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
            saveAs(blob, className+"."+lang);
            
        }
        else {
            var filepath = root + className + "." + lang
            var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
            saveAs(blob, className+"."+lang);
        }
    });

    $('.tabs .tab-links a').on('click', function(e)  {
      var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });


    // GET VALUE
    function getClassName() {
      var model = "Model";
      var name = $("#titolo").val();
        if (name.length == 0) {

            var myfile = $('#myfile')[0].files[0]
            
            name = myfile.name
            name = name.replace(/\.[^/.]+$/, "")
        }
      name = name.replace("model", model);
      name = name.camelize();
      name = name.capitalizeFirstLetter();
      var nUpp = name.search("Model");

      if (nUpp > -1)
        return name;
      else
        return name + model;
    }

    function getModelType() {
      var option = $('#modelType').val();
      
      switch (option) {
        case "Model":
          return 0;
        case "Input":
          return 1;
        case "Output":
          return 2;
        default:
          return -1;
      }
    }


    // CHECK
    function checkParams(className, json, lang) {
      var ck = true, err = "";
      if (className.length == 0) {
        ck = false;
        err = "Inserire il nome della classe";
      }
      else if (json.length == 0) {
          ck = false;
          err = "Inserire il JSON";
      }
      else if (lang == null) {
          ck = false;
          err = "Seleziona un linguaggio";
      }
      return {"success" : ck, "error" : err}
    }

};
