const T = {
    STRING: 'string',
    INT: 'number',
    BOOL: 'bool',
    DOUBLE: 'double',
    DATE: 'date',
    ARRAY: 'array',
    OBJECT: 'object',
    get: function(val){
        return t(val)
    }
}
const SWIFT = {
    t: function(type, extendedType){
        switch(type){
            case T.INT: return 'Int'
            case T.STRING: return 'String'
            case T.BOOL: return 'Bool'
            case T.DOUBLE: return 'Double'
            case T.DATE: return 'NSDate'
            case T.ARRAY: return '[' + extendedType + ']'
            default: 
            return 'AnyObject'
        }   
    }
}
const msDateRegex = /\/Date\(-?[0-9]{6,}\)\//

const json = '{"Annunci":[{"Contatti":null,"InformativaPrivacyText":null,"InformativaPrivacyLink":null,"Nome":"Bologna","IDAnnunci":60276,"DataDal":"\/Date(-62135596800000)\/","DataAl":"\/Date(-62135596800000)\/","IDTipoSelezione":0,"IDAgenteRiferimento":0,"IDAzienda":0,"IDSettoreAzienda":0,"IntroduzioneAnnuncio":null,"Introduzione1":null,"IDFiliale":0,"Introduzione2":null,"Posizione":"ADDETTI/E CASSA \u0026 ALLESTIMENTO PART-TIME (Copparo)","Annuncio":"Le Risorse saranno inserite presso punto vendita appartenente alla GDO e, sotto la supervisione del Responsabile di Negozio, si dovranno occupare di: ...","NumeroLavoratori":2,"IDLivelloIstruzione":0,"DateIns":"\/Date(-62135596800000)\/","IDUserIns":0,"DateUpd":"\/Date(-62135596800000)\/","IDUserUpd":0,"Azienda":null,"Visible":false,"IDQualificaProFISTAT":0,"QualificaProFISTAT":null,"FilialeNome":"Bologna","IdMansione":0,"StatoComunicazione":0,"StatoComunicazioneHelpLavoro":0,"StatoComunicazioneMonster":0,"IsAnnuncioTEST":false,"LuoghiLavoro":"COPPARO (FE)","IDAnnunciEnc":"MGN2UEordnBLWkU9","AziendaIsSchermata":false}],"Count":1,"TotalCount":300,"Errore":0,"Descrizione":""}'

function test(){
    parse("Nome", json, 'swift')
}

function t(val){
    if(val == null || val == 'undefined') return null
    switch(val.constructor){
        case Number:    return (val % 1 === 0) ? T.INT : T.DOUBLE
        case String:    return (val.match(msDateRegex)) ? T.DATE : T.STRING
        case Boolean:   return T.BOOL
        case Date:      return T.DATE
        case Array:     return T.ARRAY
        case Object:    return T.OBJECT
        default:        return null 
    }
}

function extractItems(obj, prefix){
    // crea il contenitore di tutti gli elementi. 
    var items = []
    items["_main_"] = []
    for(var key in obj) {
        // scorre le chiavi di primo livello (le proprietà dirette dell'oggetto)
        var subItems = null         // sotto-elementi della proprietà (per array)
        var val = obj[key]          // valore della proprietà
        var type = T.get(val)       // tipo della proprietà
        var extendedType = null
        
        if(val) {       
            // il valore non è nullo
            if(type == T.ARRAY) {
                // il tipo della proprietà è un array
                subItems = []
                
                // scorre i valori dell'array
                for(var aVal of val){
                    // estrae le proprietà dell'elemento corrente
                    var result = extractItems(aVal)
                    var aSubItems = result['_main_']
                    items[key + 'Model'] = aSubItems
                    
                    // aggiunge le proprietà ai subItems
                    subItems.push({'name': key, 'value': val, 'type': T.get(aVal), 'subItems': aSubItems, extendedType: null })
                }
                
                extendedType = key + 'Model'
            } else if (type == T.OBJECT) {
                items[key] = extractItems(val) 
            }
        }
        var finalName = key
        if (prefix != null) 
            finalName = (prefix + "." + key)
            
        items["_main_"].push({'name': finalName, 'value': val, 'type': type, 'subItems': subItems, 'extendedType': extendedType})
    }
    //console.log(items)
    return items
}

function parse(name, json, lang) {
    var obj = JSON.parse(json)
    var items = extractItems(obj)
    switch (lang) {
        case 'swift':
            return parseSwift(name, items)
        default: break
    }
}



function parseSwift(name, items) {
    var output = ''
    //console.log('public class ' + name + ' {')
    for(var key in items) {
        var className = (key == '_main_') ? name : key
        output += 'public class ' + className + ' {\n'
        var classItems = items[key]
        for(var cItem of classItems) {
            //var cItem = classItems[key]
            var p = 'public var ' + cItem.name + ':' + SWIFT.t(cItem.type, cItem.extendedType) + '?'
            //var p = 'ciao'
            output += '    ' + p + '\n'
        }
        output += '}\n\n'
    }
    return output
}
/*
exports.test = test
exports.parse = parse
exports.parseSwift = parseSwift
*/