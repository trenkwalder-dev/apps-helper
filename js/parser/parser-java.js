const JAVA = {
    t: function(type, extendedType){
        switch(type){
            case T.INT: return 'int'
            case T.STRING: return 'String'
            case T.BOOL: return 'boolean'
            case T.DOUBLE: return 'double'
            case T.DATE: return 'Date'
            case T.ARRAY: return 'List<' + extendedType + '>'
            case T.OBJECT: return extendedType
            default: 
            return 'Object'
        }   
    }
}

class JavaParser extends Parser {
    parseJava(options, items) {
        var isExtension = options.baseClassName != null && options.baseClassName != ''
        
        var output = ''
        // se non specificato diversamente inserisce le righe di import
        if(!options.noImport){
            output += 'import java.util;\n'
            output += '\n'
        }
        
        for(var key in items) {  
            // verifica se la chiave corrente corrisponde alla classe principale
            var isMain = key == '_main_'
            var isInput = isMain && options.modelType == ModelType.INPUT
            var isOutput = isMain && options.modelType == ModelType.OUTPUT
            
            // definisce il nome che avrà la classe
            var className = isMain ? options.className : key
            // definisce l'eventuale stringa di ereditarietà della classe
            var extendStr = ''
            if(isMain && options.baseClassName)
                extendStr = ' extends ' + options.baseClassName
            
            // costruisce la stringa della definizione della classe
            output += 'public class ' + className + extendStr + ' {\n'
            
            // costruisce le righe per le proprietà della classe 
            var classItems = items[key]
            for(var cItem of classItems) {
                var p = 'public ' + JAVA.t(cItem.type, cItem.extendedType) + ' ' + cItem.name + ';' + this.getNullWarning(cItem.value)
                output += '\t' + p + '\n'
            }
            
            // chiude la classe
            output += '}\n\n'
        }
        return output
    }
}