const SWIFT = {
    t: function(type, extendedType){
        switch(type){
            case T.INT: return 'Int'
            case T.STRING: return 'String'
            case T.BOOL: return 'Bool'
            case T.DOUBLE: return 'Double'
            case T.DATE: return 'NSDate'
            case T.ARRAY: return '[' + extendedType + ']'
            case T.OBJECT: return extendedType
            default: 
            return 'AnyObject'
        }   
    }
}

const SW_TYPE = {
    get: function(type){
        switch(type){
            case "number": return 'Int'
            case "int": return 'Int'
            case "string": return 'String'
            case "String": return 'String'
            case "bool": return 'Bool'
            case "double": return 'Double'
            case "date": return 'NSDate'
            case "DateTime": return 'NSDate'
            case "array": return '[' + type + ']'
            case "byte[]": return 'NSData'
            case "byte": return 'NSData'
            default: 
            return type
        }   
    }
}

class SwiftParser extends Parser {
    
    defaultInit(options) {
        var isMapping = options.map
        
        var output = ''
        // se non specificato diversamente inserisce le righe di import
        if(!options.noImport){
            output += 'import Foundation\n'
            if(isMapping)
                output += 'import ObjectMapper\n'
            output += '\n'
        }
        return output
    }
    
    
    parseSwiftFile(options, text) {
        console.log(text + "\n\n")
        var swiftParser = new SwiftParser()
        var output = new SwiftParser().defaultInit(options)    
        var lines = text.replace(/\r\n/g, "\n").split("\n");
        var arrayCampi = []
        var arrayType = []
        
        var insideClass = false
        
        lines.forEach(function(line, i) {
            if (swiftParser.lineIsAllowed(line)) {
                if (swiftParser.isClassDefinition(line) && insideClass == true) {
                    output += "}\n"
                    insideClass += false
                }
                
                if (swiftParser.isClassDefinition(line)) {
                    insideClass = true
                }
                
                var element = swiftParser.convertLine(options, line)
                output += element[0]
                arrayCampi = arrayCampi.concat(element[1])
                arrayType = arrayType.concat(element[2])
            }
        });
        
        var isInput = options.modelType == ModelType.INPUT
        var isOutput = options.modelType == ModelType.OUTPUT
        
        if (isOutput) {
           output += "\n\tpublic override func mapping(map: Map) {\n\t\tsuper.mapping(map)\n" 
           arrayCampi.forEach(function(campo){
               if (campo != null) {
                  output += "\t\t" + campo + " <- map[\"" + campo + "\"]\n"
               }
           })
           
           output += "\t}\n"
        }
        else if (isInput) {
            output += "\npublic init() {}\n"
            output += "\npublic func toParameters() -> Dictionary<String, String> {"
            output += "\nreturn [\n"
            arrayCampi.forEach(function(campo, i){
               if (campo.length > 0) {
                  output += "\t\t\"" + campo + "\": " + campo 
                  
                  var t = arrayType[i]
                  console.log(t)
                  output += (t !== "NSDate") ? "?.description" : "?.toURLString()"
                  output += " ?? \"\",\n"
               }
           })
            output += "\n]\n"
           output += "\t}\n"
        }
        
        output += "}"
        return output
    }
    
    
    convertLine(options, line) {
        var parser = new SwiftParser()
        line = parser.escapeFirstBlank(line)
            
        if (parser.isClassDefinition(line)) {
            var isInput = options.modelType == ModelType.INPUT
            var isOutput = options.modelType == ModelType.OUTPUT
            
            if (isInput) { 
                line += (line.indexOf(":") !== -1) ? "" : ": TWRichiestaProtocol"
            }
            else if (isOutput) {
                line += (line.indexOf(":") !== -1) ? "" : ": TWRispostaBaseModel"
            }
            
            if (line.indexOf("{") === -1) {
                line = line + ' {\n'
            }
            return [line, "", ""]
        }
        else if (parser.isEnum(line)) {
            line = line + ' {\n'
            return [line, "", ""]
        }
        else {
            var arrayParam = line.split(" ");
            var type = parser.getVarType(arrayParam[1])
            var par = "\t " + arrayParam[0] + " var " + arrayParam[2] + ": " + type + "?\n"
            return [par, arrayParam[2], type]
        }
    }
    
    getVarType(cType) {
        if (cType != null) {
            if (cType.indexOf("<") !== -1) {
                var start = cType.indexOf("<")
                start += 1
                var end = cType.indexOf(">")
                cType = "[" + cType.substring(start,end) + "]"
            }
            return SW_TYPE.get(cType)
        }
        
        return SW_TYPE.get("string") // default
    }
    
    escapeFirstBlank(s) {
        while(s.charAt(0) === ' ')
            s = s.substr(1);
        
        return s
    }
    
    // Controlla se la riga selezionata è la definizione della classe
    isClassDefinition(line) {
        return (line.indexOf("class") !== -1) ? true : false
    }
    
    isEnum(line) {
        return (line.indexOf("enum") !== -1) ? true : false
    }
    
    // Controlla la singola linea e dice se deve essere convertita o no
    lineIsAllowed(line) {
        line = line.trim()
        var check = true
        var arrayString = ["using", "namespace", "//", "#", "Required", "DataMapping", "if", ".Empty", "Convert"]
        arrayString.forEach(function(s) {
            if (line.indexOf(s) !== -1)  {         check = false }
            if (line.length == 0) {                check = false }
            else if (line == "{" || line == "}") { check = false }
        })
        return check
    }

    
    
    
    
    
    
    
    
    
    // JSON
    parseSwiftJson(options, items) {

        var isExtension = options.baseClassName != null && options.baseClassName != ''
        var isMapping = options.map
        var output = new SwiftParser().defaultInit(options)
        
        for(var key in items) {  
            // verifica se la chiave corrente corrisponde alla classe principale
            var isMain = key == '_main_'
            var isInput = isMain && options.modelType == ModelType.INPUT
            var isOutput = isMain && options.modelType == ModelType.OUTPUT
            
            // definisce il nome che avrà la classe
            var className = isMain ? options.className : key
            // definisce l'eventuale stringa di ereditarietà della classe
            var extendStr = ''
            if(isMain && options.baseClassName)
                extendStr = ' : ' + options.baseClassName
            else if(options.map)
                extendStr = ' : Mappable'
            
            // costruisce la stringa della definizione della classe
            output += 'public class ' + className + extendStr + ' {\n'
            
            // costruisce le righe per le proprietà della classe 
            var classItems = items[key]
            for(var cItem of classItems) {
                var p = 'public var ' + cItem.name + ':' + SWIFT.t(cItem.type, cItem.extendedType) + '?' + this.getNullWarning(cItem.value)
                output += '\t' + p + '\n'
            }
            
            if(isInput){
                // si tratta di un modello di input
                output += '\tpublic func toParameters() -> Dictionary<String,String> {\n'
                output += '\t\tvar params = Dictionary<String,String>()\n\n'
                for (var cItem of classItems){
                    var lineParamValue = '""'
                    switch(cItem.type){
                        case T.INT: lineParamValue = cItem.name + '?.description ?? ""'
                            break
                        case T.DATE: lineParamValue = cItem.name + '?.toURLString() ?? ""'
                            break
                        default: lineParamValue = cItem.name + ' ?? ""'
                            break
                    }
                    output += '\t\tparams["' + cItem.name + '"] = '
                }
                output += '\t\treturn params\n'
                output += '\t}\n\n'
            }
            else if(isMapping){
                // inserisce il codice per fare a mappatura
                output += '\n'
                if(!isExtension) {
                    output += '\tpublic required init?(_ map: Map) {\n' 
                    output += '\t\tmapping(map)\n'
                    output += '\t}\n\n'
                }
                
                output += '\tpublic ' + (isExtension ? 'override' : '') +' func mapping(map: Map) {\n' 
                if(isExtension)
                    output += '\t\tsuper.mapping(map)\n'
                    
                for(var cItem of classItems) {
                    var lineMap = (cItem.type == T.DATE)
                        ? cItem.name + ' <- (map["' + cItem.name + '"], MSDateTransform())'
                        : cItem.name + ' <- map["' + cItem.name + '"]'
                    
                    output += '\t\t' + lineMap + '\n'
                }  
                output += '\t}'
                output += '\n'
            }
            
            // chiude la classe
            output += '}\n\n'
        }
        return output
    }
}