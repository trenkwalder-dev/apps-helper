const T = {
    STRING: 'string',
    INT:    'number',
    BOOL:   'bool',
    DOUBLE: 'double',
    DATE:   'date',
    ARRAY:  'array',
    OBJECT: 'object',
    get: function(val){
        if(val == null || val == 'undefined') return null
        switch(val.constructor){
            case Number:    return (val % 1 === 0) ? T.INT : T.DOUBLE
            case String:    return (val.match(msDateRegex)) ? T.DATE : T.STRING
            case Boolean:   return T.BOOL
            case Date:      return T.DATE
            case Array:     return T.ARRAY
            case Object:    return T.OBJECT
            default:        return null 
        }
    }
}

const ModelType = {
    DEFAULT:    0,
    INPUT:      1,
    OUTPUT:     2
}

const msDateRegex = /\/Date\(-?[0-9]{6,}\)\//

const json = '{"Annunci":[{"Contatti":null,"InformativaPrivacyText":null,"InformativaPrivacyLink":null,"Nome":"Bologna","IDAnnunci":60276,"DataDal":"\/Date(-62135596800000)\/","DataAl":"\/Date(-62135596800000)\/","IDTipoSelezione":0,"IDAgenteRiferimento":0,"IDAzienda":0,"IDSettoreAzienda":0,"IntroduzioneAnnuncio":null,"Introduzione1":null,"IDFiliale":0,"Introduzione2":null,"Posizione":"ADDETTI/E CASSA \u0026 ALLESTIMENTO PART-TIME (Copparo)","Annuncio":"Le Risorse saranno inserite presso punto vendita appartenente alla GDO e, sotto la supervisione del Responsabile di Negozio, si dovranno occupare di: ...","NumeroLavoratori":2,"IDLivelloIstruzione":0,"DateIns":"\/Date(-62135596800000)\/","IDUserIns":0,"DateUpd":"\/Date(-62135596800000)\/","IDUserUpd":0,"Azienda":null,"Visible":false,"IDQualificaProFISTAT":0,"QualificaProFISTAT":null,"FilialeNome":"Bologna","IdMansione":0,"StatoComunicazione":0,"StatoComunicazioneHelpLavoro":0,"StatoComunicazioneMonster":0,"IsAnnuncioTEST":false,"LuoghiLavoro":"COPPARO (FE)","IDAnnunciEnc":"MGN2UEordnBLWkU9","AziendaIsSchermata":false}],"Count":1,"TotalCount":300,"Errore":0,"Descrizione":""}'


class Parser {
    test(){
        var data = {
            className: 'MyClass',
            //baseClassName: 'BaseClass',
            lang: 'java',
            json: json,
            map: true,
            modelType: ModelType.OUTPUT, // 0 niente, 1 input, 2 output
            type: ""
        }
        return this.parse(data)
    }

    extractSubItems(obj){
        var result = this.extractItems(obj)
        var subItems = result['_main_']
        return subItems
    }

    extractItems(obj, prefix){
        // crea il contenitore di tutti gli elementi. 
        var items = []
        items["_main_"] = []
        for(var key in obj) {
            // scorre le chiavi di primo livello (le proprietà dirette dell'oggetto)
            var subItems = null         // sotto-elementi della proprietà (per array)
            var val = obj[key]          // valore della proprietà
            var type = T.get(val)       // tipo della proprietà
            var extendedType = null
            
            if(val) {       
                // il valore non è nullo
                var subModel = key + 'Model'
                if(type == T.ARRAY) {
                    // il tipo della proprietà è un array
                    subItems = []
                    
                    // scorre i valori dell'array
                    for(var aVal of val){
                        // estrae le proprietà dell'elemento corrente
                        var aSubItems = this.extractSubItems(aVal)
                        items[subModel] = aSubItems
                        
                        // aggiunge le proprietà ai subItems
                        subItems.push({'name': key, 'value': val, 'type': T.get(aVal), 'subItems': aSubItems, extendedType: null })
                    }
                    
                    extendedType = subModel
                } else if (type == T.OBJECT) {
                    extendedType = subModel
                    items[subModel] = this.extractSubItems(val)  
                }
            }
            var finalName = key
            if (prefix != null) 
                finalName = (prefix + "." + key)
                
            items["_main_"].push({'name': finalName, 'value': val, 'type': type, 'subItems': subItems, 'extendedType': extendedType})
        }
        //console.log(items)
        return items
    }

    getNullWarning(val){
        return val == null ? '\t\t// [WARNING] Tipo non dedotto da NULL' : ''
    }
    
    parse(data){
        // className, baseClassName, lang, json, noImports, modelType
        switch (data.lang) {
            case 'swift':
                if (data.type == "json") {
                    var obj = JSON.parse(data.json)
                    var items = this.extractItems(obj)
                    return new SwiftParser().parseSwiftJson(data, items)
                }
                else if (data.type == "file") {
                    return new SwiftParser().parseSwiftFile(data, data.fileTxt)
                }
            case 'java':
                return new JavaParser().parseJava(data, items)
            default: break
        }
    }
}

//exports.parser = new Parser()