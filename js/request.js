
var INIT_URL = "https://app.trenkwalderitalia.it";
var BASE_URL = INIT_URL + "/api/";

function httpGet(theUrl, params) {

    var requestUrl = generateURL(theUrl, params);

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( params['method'], requestUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function httpGetAsync(theUrl, params, callback) {

    var requestUrl = generateURL(theUrl, params);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    if (xhr.withCredentials !== undefined) {
      // make cross-site requests
      console.log("cors");
    }

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var myArr = JSON.parse(xhr.responseText);
            callback(myArr);
        }
    }


    xhr.open(params["method"], requestUrl, false); // true for asynchronous
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('Access-Control-Allow-Methods', '*');
    xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
    xhr.setRequestHeader("Access-Control-Max-Age", "1728000");
    xhr.setRequestHeader('UserAgent', 'TWSMARTAPP');
    xhr.setRequestHeader("Cookie", "twauthcookie=BB897C8FA865B49351B698AAAF988FA43E94E64B24C7623EC6F2C76DE2B381B393CABB10A4E59D79730D600BC285DB0E74640A9A1503C73B91CF8703F1C60D46C0EC81E5F3293F98E5FB37A56DB6A32106FD8BBBC028BBB0E06C81B167B701A0BBFD18F014E73774CF53720B7D545535B401F9D4");
    xhr.send(null);
}


function generateURL(url, params) {
  var requestUrl = BASE_URL + "v" + params['APIVersion'] + "/";
  if (params['smartapp'] == true) {
      requestUrl += "SmartApp/SmartApp/";
  }
  requestUrl += url;
  console.log("REQUEST URL: " + requestUrl + "\nMETHOD: " + params['method'] );
  return requestUrl
}
