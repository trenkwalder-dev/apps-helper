// UTILS
String.prototype.camelize = function() {
  return this.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}


// RESPONSE

var margio = '<img class="img-responsive img-rounded" src="img/margio.jpg" alt="margio" />';
var ema = '<img class="img-responsive img-rounded" src="img/ema.jpg" alt="emanuele" />';

function setError(txt) {
  $(".result-img img").remove();
  var notSerious = $("#not-serious").is(':checked');
  if (notSerious == true) {
    $(".result-img").append(ema);
  }
  $("#error-text").text(txt);
  $("#save-file").prop('disabled', true);
}

function setSuccess() {
  $(".result-img img").remove();
  var notSerious = $("#not-serious").is(':checked');
  if (notSerious == true) {
    $(".result-img").append(margio);
  }
  $("#error-text").text("");
  $("#save-file").prop('disabled', false);
}
